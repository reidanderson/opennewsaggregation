﻿using System;
using System.Collections.Generic;
using Types.Wikipedia;

namespace Wikipedia
{
    public static class Parsing
    {
        private static Dictionary<char, int> hourMap = new Dictionary<char, int>()
        {
            {'A', 0},
            {'B', 1},
            {'C', 2},
            {'D', 3},
            {'E', 4},
            {'F', 5},
            {'G', 6},
            {'H', 7},
            {'I', 8},
            {'J', 9},
            {'K', 10},
            {'L', 11},
            {'M', 12},
            {'N', 13},
            {'O', 14},
            {'P', 15},
            {'Q', 16},
            {'R', 17},
            {'S', 18},
            {'T', 19},
            {'U', 20},
            {'V', 21},
            {'W', 22},
            {'X', 23}
        };

        /// <summary>
        /// Return the count of page visits for each hour.
        /// Sample: A1D1J2L2M1O1R2U4X2
        /// </summary>
        public static Page ParseHourlyCounts(string counts, Page page, DateTime date)
        {
            Dictionary<int, int> result = new Dictionary<int, int>();
            int numChars = counts.Length;

            string curNum = "";
            int? hour = null;
            foreach (char character in counts)
            {
                if (Char.IsLetter(character))
                {
                    if (!hour.HasValue)
                    {
                        hour = hourMap[character];
                    }
                    else if (hour.HasValue && !String.IsNullOrWhiteSpace(curNum))
                    {
                        result[hour.Value] = Int32.Parse(curNum);
                        curNum = "";
                        hour = hourMap[character];
                    }
                    else
                    {
                        throw new ArgumentException("Doesn't correspond to a valid hour");
                    }
                }
                else if (Char.IsDigit(character))
                {
                    curNum += character;
                }
                else
                {
                    // throw exception
                    throw new ArgumentException("Not alphanumeric");
                }
            }

            if (hour.HasValue && !String.IsNullOrWhiteSpace(curNum))
            {
                result[hour.Value] = Int32.Parse(curNum);
            }

            if (page.VisitRecords == null)
            {
                page.VisitRecords = new List<VisitRecord>();
            }

            foreach (int key in result.Keys)
            {
                VisitRecord record = new VisitRecord();
                record.RecordDateTime = date.AddHours(key);
                record.Visits = result[key];

                page.VisitRecords.Add(record);
            }

            return page;
        }
    }
}
