using Repositories;
using System;
using System.Collections.Generic;
using Types.Wikipedia;

namespace Wikipedia {
    public static class WikipediaIO {
        private static SQLiteWikiRepository _repo = new SQLiteWikiRepository(@"C:\programming\LargeFiles\Wikipedia.db");
        private static CompressionRepository _compressionRepo = new CompressionRepository();

        public static void SetRepository(string db) {
            _repo = new SQLiteWikiRepository(db);
        }

        public static void DecompressFile(string inputFile, string outputFile) {
            _compressionRepo.DecompressBz2File(inputFile, outputFile);
        }

        public static void SavePageAttributes(long pageId, DateTime pageDate, double meanVisits) {
            _repo.SavePageAttribute(AttributeType.MeanValue, pageId, pageDate, meanVisits.ToString("0.00"));
        }

        public static void SavePageVisits(Page record) {
            _repo.SavePage(record);
        }

        public static Page GetPage(int pageId) {
            return _repo.GetPage(pageId);
        }

        public static List<Page> GetTopForDay(DateTime recordDate, int topNum) {
            return _repo.GetTopForDay(recordDate, topNum);
        }

        public static long SaveAnomaly(long recordId, double score, double pValue) {
            return _repo.SaveAnomaly(recordId, score, pValue);
        }
    }
}