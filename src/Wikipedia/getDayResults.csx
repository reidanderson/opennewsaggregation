#load "Scripts/scriptIncludes.csx"
using Repositories;
using Types.Wikipedia;
using Newtonsoft.Json;
DateTime date = DateTime.Parse(Args[0]);

DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
long time = (long)(date - epoch).TotalSeconds;

SQLiteWikiRepository repo = new SQLiteWikiRepository(@"C:\programming\LargeFiles\Wikipedia.db");

List<Page> possibleNews = repo.GetEventsForDay(date);

Console.WriteLine($"News Events for {date.ToShortDateString()}:");

Console.WriteLine($"Name\tVisits\tAvg Visits");
foreach (Page page in possibleNews) {
    Console.WriteLine($"{page.PageName}\t{page.VisitRecords[0].Visits}\t{page.MeanVisits}");
}

//Console.WriteLine($"All events: {JsonConvert.SerializeObject(possibleNews)}");
Console.WriteLine($"{date.ToString("o")} in epoch: {time}");