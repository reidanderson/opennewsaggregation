#load "scriptIncludes.csx"
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Microsoft.ML;
using Microsoft.ML.Core.Data;
using Microsoft.ML.Data;
using Microsoft.ML.Runtime.Api;
using Microsoft.ML.Runtime.Data;
using Microsoft.ML.Runtime.Learners;
using Microsoft.ML.Runtime.TimeSeriesProcessing;
using Microsoft.ML.Trainers;
using Microsoft.ML.TimeSeries;
using Microsoft.ML.Transforms.Text;
using Newtonsoft.Json;
using Wikipedia;
using Types.Wikipedia;

class SsaSpikeData
{
    public float Value;

    public SsaSpikeData(float value)
    {
        Value = value;
    }
}

class SsaSpikePrediction
{
    [VectorType(3)]
    public double[] Prediction { get; set; }
}

public long RunTimeSeries(List<VisitRecord> dataList, DateTime date)
{
    long anomalyId = 0;
    var ml = new MLContext();

    const int TrainingSize = 50;
    var data = new List<SsaSpikeData>();
    foreach (VisitRecord record in dataList)
    {
        data.Add(new SsaSpikeData(record.Visits));
    }

    // Convert data to IDataView.
    var dataView = ml.CreateStreamingDataView(data);

    // Setup the SsaSpikeDetector
    var inputColumnName = nameof(SsaSpikeData.Value);
    var outputColumnName = nameof(SsaSpikePrediction.Prediction);
    var args = new SsaSpikeDetector.Arguments()
    {
        Source = inputColumnName,
        Name = outputColumnName,
        Confidence = 99,                          // The confidence for spike detection in the range [0, 100]
        PvalueHistoryLength = 8,                  // The size of the sliding window for computing the p-value; shorter windows are more sensitive to spikes.
        TrainingWindowSize = TrainingSize,        // The number of points from the beginning of the sequence used for training.
    };

    // The transformed data.
    var transformedData = new SsaSpikeEstimator(ml, args).Fit(dataView).Transform(dataView);

    // Getting the data of the newly created column as an IEnumerable of SsaSpikePrediction.
    var predictionColumn = transformedData.AsEnumerable<SsaSpikePrediction>(ml, reuseRowObject: false);

    // We only want to check the last element in predictions for an anomaly as that is the current day
    // We also only want the anomalies that are positive
    SsaSpikePrediction dayPrediction = predictionColumn.LastOrDefault();
    if (dayPrediction != null && dayPrediction.Prediction[0] == 1 && dayPrediction.Prediction[1] > 0)
    {
        Console.WriteLine($"Saved anomaly: {dataList.Last().RecordId}");
        anomalyId = WikipediaIO.SaveAnomaly(dataList.Last().RecordId, dayPrediction.Prediction[1], dayPrediction.Prediction[2]);
    }

    return anomalyId;
}

public void SavePageAttributes(Page page, DateTime dateAnalyzed) {
    double avg = 0;
    double totalVisits = 0;
    DateTime minTime = new DateTime(2038, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    DateTime maxTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

    foreach(VisitRecord vr in page.VisitRecords) {
        totalVisits += vr.Visits;
        if (vr.RecordDateTime > maxTime) {
            maxTime = vr.RecordDateTime;
        }

        if (vr.RecordDateTime < minTime) {
            minTime = vr.RecordDateTime;
        }
    }

    int numDayDifference = maxTime.Date.Subtract(minTime.Date).Days;

    // TODO this will underestimate the traffic for pages that have days with less than 50 views 
    // as those days will not be counted in totalVisits but they will be counted as a day in the range between the 
    // first and last record
    avg = totalVisits/numDayDifference;

    WikipediaIO.SavePageAttributes(page.PageId, dateAnalyzed, avg);
}

DateTime dateToAnalyze = DateTime.Parse("2018-10-01");
DateTime lastDate = DateTime.Parse("2018-12-31");

while (dateToAnalyze <= lastDate)
{
    Stopwatch stopwatch = Stopwatch.StartNew();

    List<Page> pages = WikipediaIO.GetTopForDay(dateToAnalyze, 300);

    foreach (Page page in pages)
    {
        long anomalyId = RunTimeSeries(page.VisitRecords, dateToAnalyze);
        if (anomalyId > 0) {
            SavePageAttributes(page, dateToAnalyze);
        }
    }

    stopwatch.Stop();
    Console.WriteLine($"{dateToAnalyze.ToShortDateString()}: {stopwatch.ElapsedMilliseconds}");

    dateToAnalyze = dateToAnalyze.AddDays(1);
}