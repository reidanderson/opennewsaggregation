#load "scriptIncludes.csx"
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Repositories;
using Types.Wikipedia;
using Newtonsoft.Json;

// Taken from https://github.com/fluentcassandra/fluentcassandra/blob/master/src/GuidGenerator.cs
public DateTimeOffset GetDateTimeOffset(Guid guid)
{
    DateTimeOffset GregorianCalendarStart = new DateTimeOffset(1582, 10, 15, 0, 0, 0, TimeSpan.Zero);
    int VersionByte = 7;
    int VersionByteMask = 0x0f;
    byte TimestampByte = 0;
    byte[] bytes = guid.ToByteArray();

    // reverse the version
    bytes[VersionByte] &= (byte)VersionByteMask;

    byte[] timestampBytes = new byte[8];
    Array.Copy(bytes, TimestampByte, timestampBytes, 0, 8);

    long timestamp = BitConverter.ToInt64(timestampBytes, 0);
    long ticks = timestamp + GregorianCalendarStart.Ticks;

    return new DateTimeOffset(ticks, TimeSpan.Zero);
}

public DateTime GetUtcDateTime(Guid guid)
{
    return GetDateTimeOffset(guid).UtcDateTime;
}

SQLiteWikiRepository repo = new SQLiteWikiRepository(@"C:\programming\LargeFiles\Wikipedia.db");
WikiRESTRepository apiRepo = new WikiRESTRepository();

List<Page> possibleNews = repo.GetAnomalyRecords();

string testGuid = "b54adc00-67f9-11d9-9669-0800200c9a66";
DateTime outputTime = GetUtcDateTime(new Guid(testGuid));

Console.WriteLine($"{testGuid} corresponds to: {outputTime.ToString()}");

//Console.WriteLine($"All pages: {JsonConvert.SerializeObject(possibleNews)}");

int numberOfNewsEvents = 0;
int numberOfErrors = 0;
int idx = 0;
foreach (Page anomaly in possibleNews)
{
    idx++;
    Console.WriteLine($"Processing {anomaly.PageName}: {anomaly.VisitRecords[0].Visits} views");

    // If the page starts with Special: or Talk: we want to disregard it
    if (anomaly.PageName.StartsWith("Special:") || anomaly.PageName.StartsWith("Talk:"))
    {
        continue;
    }

    // We only want the pages that have a pValue under 0.01
    if (anomaly.PValue > 0.01)
    {
        continue;
    }


    VisitRecord newsRecord = anomaly.VisitRecords.FirstOrDefault();

    if (newsRecord == null)
    {
        throw new Exception("No visit record associated to page");
    }

    try
    {
        // We are running this overnight, so let's try and be nice to Wikipedia's API
        Thread.Sleep(500);

        // We only want pages that have had references added in the past week. If they haven't, it's hard to call it news
        // We add 1 day, because we want to include any references that were added on the day of the event as well.
        List<Reference> referencesAdded = await apiRepo.ReferencesChanged(anomaly.PageName, newsRecord.RecordDateTime.AddDays(-7), newsRecord.RecordDateTime.AddDays(1));

        if (referencesAdded.Count <= 0)
        {
            continue;
        }

        if (!anomaly.AnomalyId.HasValue)
        {
            continue;
        }

        repo.SaveEvent(anomaly.PageId, anomaly.AnomalyId.Value, referencesAdded);
        Console.WriteLine($"Would save anomaly {anomaly.AnomalyId} from day {anomaly.VisitRecords[0].RecordDateTime.ToShortDateString()}");

        numberOfNewsEvents++;
        Console.WriteLine($"Out of {idx} anomalies, we estimate {numberOfNewsEvents} real news events before {newsRecord.RecordDateTime.ToShortDateString()}");
    }
    catch (Exception ex)
    {
        numberOfErrors++;
        Console.WriteLine($"Something went wrong: {ex.ToString()}");
    }
}

Console.WriteLine($"Out of {idx} anomalies, we estimate {numberOfNewsEvents} real news events before");

repo.SaveEvent(0,0,null, false);

Console.WriteLine($"Out of {possibleNews.Count} anomalies, we estimate {numberOfNewsEvents} news events. There were {numberOfErrors} errors");