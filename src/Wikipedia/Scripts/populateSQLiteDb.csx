#load "scriptIncludes.csx"
using Wikipedia;
using Types.Wikipedia;
using System.IO;
using Newtonsoft.Json;
Console.WriteLine("Hello world!");

string inputFolder = @"G:\WikipediaLogs";
string outputFolder = @"C:\programming\LargeFiles\wiki";

DirectoryInfo d = new DirectoryInfo(inputFolder);//Assuming Test is your Folder
FileInfo[] Files = d.GetFiles("*.bz2"); //Getting zip files
foreach (FileInfo file in Files)
{
    string baseFileName = Path.GetFileNameWithoutExtension(file.Name);
    DateTime date = DateTime.Parse(baseFileName.Substring(baseFileName.Length - 10));    
    string input = file.FullName;
    string output = outputFolder + "/" + baseFileName + ".log";
    //WikipediaIO.DecompressFile(input, output);

    var filestream = new System.IO.FileStream(output,
                                              System.IO.FileMode.Open,
                                              System.IO.FileAccess.Read,
                                              System.IO.FileShare.ReadWrite);
    var decomFile = new System.IO.StreamReader(filestream, System.Text.Encoding.UTF8, true, 128);

    int lineNumber = 0;
    string lineOfText = "";
    Stopwatch stopwatch = Stopwatch.StartNew();
    while ((lineOfText = decomFile.ReadLine()) != null)
    {
        if (lineOfText[0] == '#')
        {
            continue;
        }

        string[] components = lineOfText.Split(' ');

        string[] productComp = components[0].Split('.');

        string country = productComp[0];
        string component = null;

        if (productComp.Length > 1)
        {
            component = productComp[1];
        }

        int dailyTotal = 0;
        if (!Int32.TryParse(components[2], out dailyTotal))
        {
            Console.WriteLine("Daily total not valid int");
            continue;
            throw new Exception("Daily total not valid int");
        }

        if (dailyTotal < 50 || country != "en" || (component != "z" && component != "m") || components[1].Contains("File:"))
        {
            continue;
        }

        lineNumber++;

        if (lineNumber % 100000 == 0)
        {
            Console.WriteLine($"Lines processed - {lineNumber}: {lineOfText}");
        }

        Page record = new Page()
        {
            Language = country,
            Project = component,
            PageName = components[1],
            VisitRecords = new List<VisitRecord>() { new VisitRecord { RecordDateTime = date, Visits = Int32.Parse(components[2]) } }
        };

        //Page result = Parsing.ParseHourlyCounts(components[3], record, date);

        //WikipediaIO.SavePageVisits(record);
    }

    decomFile.Close();
    filestream.Close();

    File.Delete(output);

    stopwatch.Stop();
    Console.WriteLine($"{file.FullName}: {stopwatch.ElapsedMilliseconds}");
}