using System.Net;

WebClient Client = new WebClient();

//List<string> months = new List<string>() { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
// Let's start with the last 6 months
List<string> months = new List<string>() { "09", "10", "11", "12" };

Dictionary<string, int> daysOfMonth = new Dictionary<string, int>() {
    {"01", 31},
    {"02", 28},
    {"03", 31},
    {"04", 30},
    {"05", 31},
    {"06", 30},
    {"07", 31},
    {"08", 31},
    {"09", 30},
    {"10", 31},
    {"11", 30},
    {"12", 31},
};

foreach (string month in months)
{

    for (int day = 1; day <= daysOfMonth[month]; day++)
    {
        string paddedDay = day.ToString();

        if (day < 10) {
            paddedDay = "0"+day.ToString();
        }

        string fileName = $"pagecounts-2018-{month}-{paddedDay}.bz2";
        
        string folderPath = @"G:\WikipediaLogs\"+fileName;
        string urlPath = $"https://dumps.wikimedia.org/other/pagecounts-ez/merged/2018/2018-{month}/{fileName}";

        Console.WriteLine($"Downloading from {urlPath} to {folderPath}");
        Client.DownloadFile(urlPath, folderPath);
    }

}

