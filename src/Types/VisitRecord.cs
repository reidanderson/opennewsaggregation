﻿using System;

namespace Types.Wikipedia
{
    public class VisitRecord
    {
        public long RecordId { get; set; }
        public DateTime RecordDateTime { get; set; }
        public int Visits { get; set; }
    }
}
