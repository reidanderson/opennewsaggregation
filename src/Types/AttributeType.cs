namespace Types.Wikipedia
{
    public enum AttributeType
    {
        MeanValue = 1,
        CreatedDate = 2
    }
}