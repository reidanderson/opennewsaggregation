using System;

namespace Types.Wikipedia
{
    public class Reference
    {
        public int EventReferenceId { get; set; }
        public string Code { get; set; }
        public string Content { get; set; }
    }
}
