using System;
using System.Collections.Generic;

namespace Types.Wikipedia
{
    public class Page
    {
        public long PageId { get; set; }
        public string PageName { get; set; }
        public string Language { get; set; }
        public string Project { get; set; }
        public double MeanVisits { get; set; }
        public double PValue { get; set; }
        public double Score { get; set; }
        public long? AnomalyId { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<VisitRecord> VisitRecords { get; set; }
    }
}
