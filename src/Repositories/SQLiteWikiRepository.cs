﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using Types.Wikipedia;

namespace Repositories
{
    public class SQLiteWikiRepository
    {
        private string _db = null;
        private List<Page> currentPageBatch = new List<Page>();
        private List<Tuple<long, long, List<Reference>>> currentEventBatch = new List<Tuple<long, long, List<Reference>>>();

        public SQLiteWikiRepository(string dbLoc)
        {
            _db = dbLoc;
        }

        public List<Page> GetEventsForDay(DateTime day)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long time = (long)(day - epoch).TotalSeconds;
            List<Page> result = new List<Page>();

            using (SQLiteConnection connection = new SQLiteConnection($"Data Source={_db};Version=3;"))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    string selectSql = @"SELECT * FROM Event AS E INNER JOIN Anomalies AS A ON E.AnomalyId = A.AnomalyId INNER JOIN VisitRecords AS VR ON A.RecordId=VR.RecordId INNER JOIN PageNames AS PN ON VR.PageId=PN.PageId INNER JOIN PageAttributes AS PA ON PN.PageId=PA.PageId WHERE VR.RecordDate=$recordDate AND PA.PageDate=$recordDate ORDER BY VR.Visits DESC";
                    SQLiteCommand selectCommand = new SQLiteCommand(selectSql, connection);
                    selectCommand.Parameters.Add("$recordDate", DbType.Int32).Value = time;

                    SQLiteDataReader reader = selectCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        Page page = new Page();
                        page.PageName = Convert.ToString(reader["Page"]);
                        page.Language = Convert.ToString(reader["WikiShortcode"]);
                        page.PageId = Int64.Parse(Convert.ToString(reader["PageId"]));
                        page.MeanVisits = Double.Parse(Convert.ToString(reader["Value"]));
                        page.PValue = Double.Parse(Convert.ToString(reader["PValue"]));

                        VisitRecord record = new VisitRecord();
                        record.RecordId = Int64.Parse(Convert.ToString(reader["RecordId"]));
                        record.RecordDateTime = epoch.AddSeconds(Int64.Parse(Convert.ToString(reader["RecordDate"])));
                        record.Visits = Int32.Parse(Convert.ToString(reader["Visits"]));
                        page.VisitRecords = new List<VisitRecord>() { record };

                        result.Add(page);
                    }
                }
            }

            return result;
        }

        public List<Page> GetTopForDay(DateTime day, int articleLimit)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long time = (long)(day - epoch).TotalSeconds;
            List<Page> topResults = new List<Page>();
            Dictionary<long, Page> pageDict = new Dictionary<long, Page>();
            using (SQLiteConnection connection = new SQLiteConnection($"Data Source={_db};Version=3;"))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    string selectPageSql = @"SELECT * FROM VisitRecords AS VR WHERE RecordDate<=$recordDate AND PageId IN (SELECT PageId FROM VisitRecords AS VR2 WHERE RecordDate=$recordDate ORDER BY Visits DESC LIMIT $articleLimit)";
                    SQLiteCommand selectCommand = new SQLiteCommand(selectPageSql, connection);
                    selectCommand.Parameters.Add("$recordDate", DbType.Int32).Value = time;
                    selectCommand.Parameters.Add("$articleLimit", DbType.Int32).Value = articleLimit;

                    SQLiteDataReader reader = selectCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        long pageId = Int64.Parse(Convert.ToString(reader["PageId"]));

                        if (pageDict.ContainsKey(pageId))
                        {
                            VisitRecord record = new VisitRecord();
                            record.RecordId = Int64.Parse(Convert.ToString(reader["RecordId"]));
                            record.RecordDateTime = epoch.AddSeconds(Int64.Parse(Convert.ToString(reader["RecordDate"])));
                            record.Visits = Int32.Parse(Convert.ToString(reader["Visits"]));
                            pageDict[pageId].VisitRecords.Add(record);
                        }
                        else
                        {
                            Page page = new Page();
                            page.VisitRecords = new List<VisitRecord>();
                            page.PageId = pageId;
                            VisitRecord record = new VisitRecord();
                            record.RecordId = Int64.Parse(Convert.ToString(reader["RecordId"]));
                            record.RecordDateTime = epoch.AddSeconds(Int64.Parse(Convert.ToString(reader["RecordDate"])));
                            record.Visits = Int32.Parse(Convert.ToString(reader["Visits"]));
                            page.VisitRecords.Add(record);
                            pageDict[pageId] = page;
                        }
                    }
                }
            }

            foreach (Page page in pageDict.Values)
            {
                page.VisitRecords = page.VisitRecords.OrderBy(vr => vr.RecordDateTime).ToList();
                topResults.Add(page);
            }

            return topResults;
        }

        public void SaveEvent(long pageId, long anomalyId, List<Reference> referencesAdded, bool batch = true)
        {
            if (batch)
            {
                currentEventBatch.Add(new Tuple<long, long, List<Reference>>(pageId, anomalyId, referencesAdded));
            }

            if (currentEventBatch.Count < 100 && batch)
            {
                return;
            }

            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            using (SQLiteConnection connection = new SQLiteConnection($"Data Source={_db};Version=3;"))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    foreach (Tuple<long, long, List<Reference>> eventObj in currentEventBatch)
                    {
                        string InsertAnomaly = @"INSERT INTO Event 
                                    (PageId, AnomalyId) 
                                    values ($pageId, $anomalyId)";

                        Console.WriteLine($"InsertEvent:{eventObj.Item1},{eventObj.Item2},{eventObj.Item3.Count}");

                        SQLiteCommand pageCommand = new SQLiteCommand(InsertAnomaly, connection);
                        pageCommand.Parameters.Add("$pageId", DbType.Int64).Value = eventObj.Item1;
                        pageCommand.Parameters.Add("$anomalyId", DbType.Int64).Value = eventObj.Item2;

                        pageCommand.ExecuteNonQuery();

                        long eventId = connection.LastInsertRowId;

                        foreach (Reference refer in eventObj.Item3)
                        {
                            string insertReference = @"INSERT INTO EventReferences 
                                    (EventId, ReferenceCode, ReferenceContent) 
                                    values ($eventId, $referenceCode, $referenceContent)";

                            SQLiteCommand refCommand = new SQLiteCommand(insertReference, connection);
                            refCommand.Parameters.Add("$eventId", DbType.Int64).Value = eventId;
                            refCommand.Parameters.Add("$referenceCode", DbType.String).Value = refer.Code;
                            refCommand.Parameters.Add("$referenceContent", DbType.String).Value = refer.Content;

                            refCommand.ExecuteNonQuery();
                        }
                    }

                    transaction.Commit();
                }
            }

            currentEventBatch = new List<Tuple<long, long, List<Reference>>>();
        }

        public void SavePageAttribute(AttributeType attributeType, long pageId, DateTime? pageDate, string value)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            using (SQLiteConnection connection = new SQLiteConnection($"Data Source={_db};Version=3;"))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    string InsertAnomaly = @"INSERT INTO PageAttributes 
                                    (PageId, AttributeTypeId, Value, PageDate) 
                                    values ($pageId, $attributeTypeId, $value, $pageDate)";

                    SQLiteCommand pageCommand = new SQLiteCommand(InsertAnomaly, connection);
                    pageCommand.Parameters.Add("$pageId", DbType.Int32).Value = pageId;
                    pageCommand.Parameters.Add("$attributeTypeId", DbType.Int32).Value = (int)attributeType;
                    pageCommand.Parameters.Add("$value", DbType.String).Value = value;

                    if (pageDate.HasValue)
                    {
                        pageCommand.Parameters.Add("$pageDate", DbType.Int64).Value = (long)(pageDate - epoch).Value.TotalSeconds;
                    }
                    else
                    {
                        pageCommand.Parameters.Add("$pageDate", DbType.Int64).Value = DBNull.Value;
                    }

                    pageCommand.ExecuteNonQuery();

                    transaction.Commit();
                }

            }
        }

        public List<Page> GetAnomalyRecords(DateTime? date = null)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            List<Page> pages = new List<Page>();
            using (SQLiteConnection connection = new SQLiteConnection($"Data Source={_db};Version=3;"))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    string selectPageSql = @"SELECT * FROM Anomalies AS A
                        INNER JOIN VisitRecords AS VR ON A.RecordId=VR.RecordId
                        INNER JOIN PageNames AS PN ON VR.PageId=PN.PageId ";
                    if (date.HasValue)
                    {
                        selectPageSql += "WHERE VR.RecordDate=$date";
                    }
                    SQLiteCommand selectCommand = new SQLiteCommand(selectPageSql, connection);

                    if (date.HasValue)
                    {
                        selectCommand.Parameters.Add("$pageId", DbType.Int32).Value = (long)(date.Value - epoch).TotalSeconds;
                    }


                    SQLiteDataReader reader = selectCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        Page page = new Page();
                        page.VisitRecords = new List<VisitRecord>();
                        page.PageName = Convert.ToString(reader["Page"]);
                        page.PageId = Int64.Parse(Convert.ToString(reader["PageId"]));
                        page.Language = Convert.ToString(reader["WikiShortcode"]);
                        page.AnomalyId = Int64.Parse(Convert.ToString(reader["AnomalyId"]));
                        VisitRecord record = new VisitRecord();
                        record.RecordDateTime = epoch.AddSeconds(Int64.Parse(Convert.ToString(reader["RecordDate"])));
                        record.Visits = Int32.Parse(Convert.ToString(reader["Visits"]));
                        page.VisitRecords.Add(record);
                        page.VisitRecords.Add(record);
                        pages.Add(page);
                    }
                }
            }

            return pages;
        }

        public Page GetPage(int pageId)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            Page page = new Page();
            page.VisitRecords = new List<VisitRecord>();
            using (SQLiteConnection connection = new SQLiteConnection($"Data Source={_db};Version=3;"))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    string selectPageSql = @"SELECT RecordDate, Visits FROM VisitRecords WHERE pageId=$pageId";
                    SQLiteCommand selectCommand = new SQLiteCommand(selectPageSql, connection);
                    selectCommand.Parameters.Add("$pageId", DbType.Int32).Value = pageId;

                    SQLiteDataReader reader = selectCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        VisitRecord record = new VisitRecord();
                        record.RecordId = Int64.Parse(Convert.ToString(reader["RecordId"]));
                        record.RecordDateTime = epoch.AddSeconds(Int64.Parse(Convert.ToString(reader["RecordDate"])));
                        record.Visits = Int32.Parse(Convert.ToString(reader["Visits"]));
                        page.VisitRecords.Add(record);
                    }
                }
            }

            return page;
        }

        public long SaveAnomaly(long recordId, double score, double pValue)
        {
            long anomalyId = 0;
            using (SQLiteConnection connection = new SQLiteConnection($"Data Source={_db};Version=3;"))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    string InsertAnomaly = @"INSERT INTO Anomalies 
                                    (RecordId, Score, pValue) 
                                    values ($recordId, $score, $pValue)";

                    SQLiteCommand pageCommand = new SQLiteCommand(InsertAnomaly, connection);
                    pageCommand.Parameters.Add("$recordId", DbType.Int64).Value = recordId;
                    pageCommand.Parameters.Add("$score", DbType.Double).Value = score;
                    pageCommand.Parameters.Add("$pValue", DbType.Double).Value = pValue;

                    pageCommand.ExecuteNonQuery();

                    anomalyId = connection.LastInsertRowId;

                    transaction.Commit();
                }

            }

            return anomalyId;
        }

        public void SavePage(Page page)
        {
            currentPageBatch.Add(page);

            if (currentPageBatch.Count > 50000)
            {
                SaveRecordsForPage(currentPageBatch);
                currentPageBatch = new List<Page>();
            }
        }

        public void SaveRecordsForPage(List<Page> pages)
        {
            using (SQLiteConnection connection = new SQLiteConnection($"Data Source={_db};Version=3;"))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    foreach (Page page in pages)
                    {
                        try
                        {
                            string selectPageSql = @"SELECT PageId FROM PageNames WHERE WikiShortcode=$shortCode AND Page=$pageName";
                            SQLiteCommand selectCommand = new SQLiteCommand(selectPageSql, connection);
                            selectCommand.Parameters.Add("$shortCode", DbType.String).Value = $"{page.Language}";
                            selectCommand.Parameters.Add("$pageName", DbType.String).Value = page.PageName;

                            SQLiteDataReader reader = selectCommand.ExecuteReader();

                            if (reader.Read())
                            {
                                page.PageId = Int64.Parse(Convert.ToString(reader["PageId"]));
                            }
                            else
                            {
                                string InsertPageSql = @"INSERT OR REPLACE INTO PageNames 
                                    (WikiShortcode, Page) 
                                    values ($shortCode, $pageName)";

                                SQLiteCommand pageCommand = new SQLiteCommand(InsertPageSql, connection);
                                pageCommand.Parameters.Add("$shortCode", DbType.String).Value = $"{page.Language}";
                                pageCommand.Parameters.Add("$pageName", DbType.String).Value = page.PageName;

                                pageCommand.ExecuteNonQuery();

                                page.PageId = connection.LastInsertRowId;
                            }

                            foreach (VisitRecord record in page.VisitRecords)
                            {
                                var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                                var time = (long)(record.RecordDateTime - epoch).TotalSeconds;
                                string InsertSql = @"INSERT INTO VisitRecords 
                                    (PageId, RecordDate, Visits) 
                                    values ($pageId, $time, $visits) ON CONFLICT(PageId, RecordDate) DO UPDATE SET Visits=Visits+$visits";

                                SQLiteCommand command = new SQLiteCommand(InsertSql, connection);
                                command.Parameters.Add("$pageId", DbType.Int64).Value = page.PageId;
                                command.Parameters.Add("$time", DbType.Int64).Value = time;
                                command.Parameters.Add("$visits", DbType.Int32).Value = record.Visits;
                                command.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"DB write failed: {ex}");
                        }

                    }

                    transaction.Commit();
                }
            }
        }
    }
}
