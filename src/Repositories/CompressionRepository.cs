using System;
using System.IO;
using System.IO.Compression;
using ICSharpCode.SharpZipLib.BZip2;

namespace Repositories
{

    public class CompressionRepository
    {
        public void DecompressZipFolder(string inputLocation, string outputLocation)
        {
            Console.WriteLine("Decompressing folder");
            // Find all zip files in this location
            DirectoryInfo d = new DirectoryInfo(@inputLocation);//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.zip"); //Getting zip files
            string str = "";
            foreach (FileInfo file in Files)
            {
                ZipFile.ExtractToDirectory(file.FullName, outputLocation + "/" + file.Name);
                str = str + ", " + file.Name;
            }
        }

        public void DecompressBz2File(string inputFile, string outputFile)
        {
            FileInfo inputFileInfo = new FileInfo(inputFile);
            using (FileStream fileToDecompressAsStream = inputFileInfo.OpenRead())
            {
                using (FileStream decompressedStream = File.Create(outputFile))
                {
                    try
                    {
                        BZip2.Decompress(fileToDecompressAsStream, decompressedStream, true);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        public void DecompressBz2Folder(string inputLocation, string outputLocation)
        {
            Console.WriteLine("Decompressing folder");
            // Find all zip files in this location
            DirectoryInfo d = new DirectoryInfo(@inputLocation);//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.bz2"); //Getting bz2 files
            foreach (FileInfo file in Files)
            {
                DecompressBz2File(file.FullName, outputLocation + "/" + file.Name);
            }
        }
    }
}