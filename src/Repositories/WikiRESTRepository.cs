using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Types.Wikipedia;

namespace Repositories
{
    public class WikiRESTRepository
    {
        private HttpClient _client;

        public WikiRESTRepository()
        {
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Add("Api-User-Agent", "throwfaraway0@gmail.com");
        }

        public List<Reference> GetNewReferences(JObject referenceObject, List<string> referenceCodes) {
            List<Reference> result = new List<Reference>();

            JObject referencesByCode = (JObject) referenceObject["references_by_id"];
            
            if (referencesByCode == null) {
                return result;
            }

            foreach(string referenceCode in referenceCodes) {
                Reference reference = new Reference();
                reference.Code = referenceCode;
                reference.Content = referencesByCode[referenceCode]["content"].ToString();
                result.Add(reference);
            }

            return result;
        }

        public List<string> GetReferenceList(JObject referenceObject)
        {
            List<string> result = new List<string>();
            JArray referenceLists = (JArray)referenceObject["reference_lists"];

            if (referenceLists == null)
            {
                Console.WriteLine("No references");
                return result;
            }

            foreach (JObject refList in referenceLists)
            {
                if (refList["order"] == null)
                {
                    Console.WriteLine("No reference order");
                    return result;
                }

                foreach (string reference in refList["order"])
                {
                    result.Add(reference);
                }
            }

            return result;
        }

        public async Task<List<Reference>> ReferencesChanged(string pageName, DateTime startDate, DateTime endDate)
        {
            HashSet<string> set = new HashSet<string>();
            List<Reference> newReferences = new List<Reference>();
            string revId = (await GetRevisionIdForDate(pageName, startDate)).ToString();
            string newerRevId = (await GetRevisionIdForDate(pageName, endDate)).ToString();

            // If both dates have the same revision date, nothing has changed
            if (revId == "0" || newerRevId == "0" || newerRevId == revId)
            {
                return newReferences;
            }

            string referenceUrl = @"https://en.wikipedia.org/api/rest_v1/page/references";
            HttpResponseMessage response = await _client.GetAsync($"{referenceUrl}/{pageName}/{revId}");
            string responseContent = await response.Content.ReadAsStringAsync();

            JObject responseObject = JObject.Parse(responseContent);

            List<string> referenceList = GetReferenceList(responseObject);

            int initialReferences = 0;
            foreach(string reference in referenceList) {
                set.Add(reference);
                initialReferences++;
            }

            ////// Now do it for the more recent version of the page

            referenceUrl = @"https://en.wikipedia.org/api/rest_v1/page/references";
            response = await _client.GetAsync($"{referenceUrl}/{pageName}/{newerRevId}");
            responseContent = await response.Content.ReadAsStringAsync();

            responseObject = JObject.Parse(responseContent);

            referenceList = GetReferenceList(responseObject);

            List<string> newReferenceCodes = new List<string>();
            foreach (string reference in referenceList) {
                if (!set.Contains(reference)) {
                    newReferenceCodes.Add(reference);
                }
            }

            newReferences = GetNewReferences(responseObject, newReferenceCodes);

            //Console.WriteLine($"Number of references: {initialReferences}");
            //Console.WriteLine($"Number of new references: {newReferences.Count}");

            return newReferences;
        }

        public async Task<int> GetRevisionIdForDate(string pageName, DateTime date)
        {
            int revisionId = 0;

            string formattedDate = date.ToString("o");
            string baseRevisionApi = "https://en.wikipedia.org/w/api.php?format=json&rvstart=";
            string pageRevisionEndpoint = $"{baseRevisionApi}{formattedDate}&rvdir=older&rvprop=ids%7Ctimestamp%7Cuser%7Ccomment&rvslots=main&rvlimit=1&titles={pageName}&prop=revisions&action=query";
            HttpResponseMessage response = await _client.GetAsync(pageRevisionEndpoint);
            string responseContent = await response.Content.ReadAsStringAsync();

            JObject responseObject = JObject.Parse(responseContent);

            JToken pages = responseObject["query"]["pages"];

            var pageObject = pages.First.First;

            if (pageObject["revisions"] == null || pageObject["revisions"][0] == null || pageObject["revisions"][0]["revid"] == null)
            {
                Console.WriteLine("No revisions, nothing to see here");
                return 0;
            }
            string revId = pageObject["revisions"][0]["revid"].ToString();
            string timestamp = pageObject["revisions"][0]["timestamp"].ToString();

            //Console.WriteLine($"Revision: {revId}");

            if (revId == null || !Int32.TryParse(revId, out revisionId))
            {
                throw new Exception("Could not parse revision id to number");
            }

            return revisionId;
        }
    }
}